#### Instructions
- Let's have some web page hosted, on which we can enable this monitoring
- Go to vm and let's install `apache2`

    `apt-get install apache2`
- Moreover, allow the traffic on port no. `80` on the VM, as apache2 runs on that port
- Get the code
    
    ```git clone https://gitlab.com/konnectchetan/azure-applicationinsights-clientside-javascript```
- Replace the default html file with the updated one

    ```
    rm /var/www/html/index.html
    mv azure-applicationinsights-clientside-javascript/index.html /var/www/html/
    ```
- Edit the `index.html` file and udpate your Instrumentation Key of Application Insights over there

    ```
    cd /var/www/html
    vi index.html
    ```
- Now, browse your web page, give him some hits, and post it go to Application Insights page, and under `Performance` tab, look for `browser` insights, there you will see your data popping out.
- To see all the records, in the left pane, click on `Logs`, a pop-up will appear, just close it, and run the query saying

    `pageViews`
